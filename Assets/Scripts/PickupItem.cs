using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupItem : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))  // if Player touched the item
        {
            other.GetComponent<PlayerController>().stats.float_ObjectHealth += 50;  // Give player 50 hp
            gameObject.SetActive(false);  // close object so 1 pickup only
        }
    }
}
