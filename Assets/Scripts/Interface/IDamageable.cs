using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDamageable  // interface for damageble objects
{
    void DamageObject(float _damageAmount);
}