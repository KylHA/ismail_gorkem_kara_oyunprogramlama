using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPlayable // for Level desing (not Used)
{
    void OnGameLoad();
    void OnGameStart();
}
