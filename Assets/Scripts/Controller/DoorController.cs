using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorController : MonoBehaviour
{
    [SerializeField] List<EnemyController> enemyControllers;  // list for triggering enemies when door opened

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))  // if Player touched the door
        {
            GetComponent<Animator>().Play("Open");  // Open door
            enemyControllers.ForEach(x => { x.isPassive = false; x.OverrideUpdate(); });  // Trigger enemy fire func 
        }
    }
}