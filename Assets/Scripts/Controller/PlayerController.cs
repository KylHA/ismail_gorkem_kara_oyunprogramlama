using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour, IPlayable, IDamageable
{
    public Stats stats;  // Stat of player

    private Animator m_Animator;  // player animator reference

    public void DamageObject(float _damageAmount)  // IDamageable interface func. for damaging objects
    {
        stats.float_ObjectHealth -= _damageAmount;   // substract damage from health
        CheckObjectStatus();  // Check status after damage
    }
    private void CheckObjectStatus()
    {
        if (stats.float_ObjectHealth <= 0) // if health equals or below zero
        {
            ReLoadGame();  // Restart Game
        }
    }

    public void OnGameLoad()
    {
    }

    public void OnGameStart()
    {
    }

    public void ReLoadGame() 
    {
        SceneManager.UnloadSceneAsync(0);  // Unload Scene
        SceneManager.LoadSceneAsync(0);     // "Re" Load Scene
    }
}
