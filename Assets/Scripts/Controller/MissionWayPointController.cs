using UnityEngine;

public class MissionWayPointController : MonoBehaviour
{
    [SerializeField] private GameObject gameObject_UI_TextInteract;  // UI text tutorial for keyboard 'E' action
    [SerializeField] private Transform transform_Player_Level_2_refTransform;  //  transform of Player next level teleport
    [SerializeField] private GameObject level_2,level_1;  // correponding level objects for openenin and closing
    private Waypoint_Indicator my_waypoint_Indicator;  // waypoint indicator reference

    private Transform transform_Player;  // Game Player transform reference

    private bool isPlayerInteracting = false; // Is Player currently touching waypoint

    private void Awake() 
    {
        my_waypoint_Indicator = GetComponent<Waypoint_Indicator>();  // Get Reference
        transform_Player = GameObject.FindGameObjectWithTag("Player").transform; // Get Reference
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player")) // if Player touched the waypoint
        {
            gameObject_UI_TextInteract.SetActive(true);  // Open UI tutorial
            isPlayerInteracting = true;  // Player is currently touching waypoint
        }
    }

    public void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player")) // if Player stopped touched the waypoint
        {
            gameObject_UI_TextInteract.SetActive(false); // Close UI tutorial
            isPlayerInteracting = false; // Player is  not currently touching waypoint
        }
    }

    private void Update() 
    {
        if (!isPlayerInteracting) // Check  is Player currently touching waypoint
            return;

        if (Input.GetKeyDown(KeyCode.E))  // Get Keyboard 'E' input
        {
            gameObject_UI_TextInteract.SetActive(false);  // Close UI tutorial
            transform_Player.parent = level_2.transform;  // Set Player Parent
            level_2.SetActive(true);                      // Open Next Level
            isPlayerInteracting = false;                  // Close Player waypoint check
            my_waypoint_Indicator.enabled = false;        // Close UI Waypoint
            transform_Player.position = transform_Player_Level_2_refTransform.position; // Teleport Player
            transform_Player.rotation = transform_Player_Level_2_refTransform.rotation; // Rotate Player
            level_1.SetActive(false);   // Close Current Level
        }
    }

}