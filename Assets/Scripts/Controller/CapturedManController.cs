using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CapturedManController : MonoBehaviour
{
    [SerializeField] GameObject interactText;
    private bool isInteracting;



    private void Update()
    {
        if (!isInteracting)
            return;

        if (Input.GetKeyDown(KeyCode.E)) 
        {
            interactText.SetActive(false);
            MenuController.instance.SetCredits();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player")) 
        {
            interactText.SetActive(true);
            isInteracting = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            interactText.SetActive(false);
            isInteracting = false;
        }
    }
}
