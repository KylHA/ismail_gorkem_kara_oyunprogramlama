using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyController : MonoBehaviour, IPlayable, IDamageable
{
    [SerializeField] Stats stats;                               // Stat of enemy
    [SerializeField] private Transform transform_Gun;           // Gun transform reference
    [SerializeField] private Transform transform_gunIdle;       // Gun Idle transform reference
    [SerializeField] private Transform transform_gunAction;     // Gun Action transform reference
    [SerializeField] private Transform transform_RayOrigin;     // Transform for Player check (isVisible)

    public GameObject muzzelSpawn;                              // Muzzle flash Instance object
    private GameObject holdFlash;                               // Muzzle flash pos holder
    public GameObject[] muzzelFlash;                            // Spawned muzzle flash array

    public GameObject Player;                                   // Player object Reference for following

    public float AttackDistance = 10.0f;                        // Enemy Attack Dist.
    public float FollowDistance = 20.0f;                        // Enemy Follow Dist.
    [Range(0.0f, 1.0f)] public float AttackProbability = 0.5f;  // percentage of AttackProbability
    [Range(0.0f, 1.0f)] public float HitAccuracy = 0.5f;        // percentage of Accuracy
    public float DamagePoints = 2.0f;                           // Damage to player when player hit
    public AudioSource GunSound = null;                         // Sound when firing

    private Animator _animator;                                 // Enemy animator reference
    private NavMeshAgent _navMeshAgent;                         // Unity A* movement AI referance
    private bool ovverideAction;                                // Is Unity Update tolerated
    public  bool isPassive=false;                                // Is Unity Update tolerated

    private void Awake()
    {
        OnGameLoad();
        OnGameStart();
    }

    public void OnGameLoad()
    {
        gameObject.SetActive(true);                             // Open object if closed

        _navMeshAgent = GetComponent<NavMeshAgent>();           // Get referance

        _animator = GetComponent<Animator>();                   // Get referance

        ovverideAction = false;                                 // Unity Update is tolerated

    }

    public void OnGameStart()
    {
    }

    public void DamageObject(float _damageAmount)               // IDamageable interface func. for damaging objects
    {
        stats.float_ObjectHealth -= _damageAmount;              // substract damage from health
        CheckAttentionStatus();                                 // Alert on damage if not alerted
        CheckObjectStatus();                                    // Check status after damage
    }

    private void CheckAttentionStatus()
    {
        float dist = Vector3.Distance(Player.transform.position, this.transform.position);  // Chec dist. to player
        if (dist > AttackDistance)                                      // if far then attack range
        {
            _navMeshAgent.SetDestination(Player.transform.position);    // Move close to player
            GunToAction();                                              // set gun action pos
        }
    }

    public void OverrideUpdate()
    {
        ovverideAction = true;  // ovveride Unity update
    }

    private void Update()
    {
        if (isPassive) return;

        float dist = Vector3.Distance(Player.transform.position, this.transform.position); // Chec dist. to player

        if (ovverideAction) // if ovveridden
        {
            transform.LookAt(Player.transform);                                 // Look at player

            Ray ray = new Ray(transform_RayOrigin.position, (Player.transform.position - transform_RayOrigin.position).normalized); // ray to look player
            RaycastHit hit;
            ray.direction *= 100;

            _animator.SetBool("Idle", false);
            if (dist > AttackDistance)                                          // if far then attack range
            {
                _navMeshAgent.SetDestination(Player.transform.position);        // Move close to player
                _animator.SetBool("Shoot", false);
                _animator.SetBool("Run", true);
                GunToAction();                                                  // set gun action pos
            }
            else if (Physics.Raycast(ray, out hit))                              // Cast ray
            {
                _navMeshAgent.SetDestination(transform.position);               // Stop moving
                GunToAction();

                if (!hit.collider.CompareTag("Player"))                         // if ray doesn't hit player animator run
                {
                    _animator.SetBool("Shoot", false);
                    _animator.SetBool("Run", true);
                    _navMeshAgent.SetDestination(Player.transform.position);
                }
                else                                                            // if rayhit player animator shoot
                {
                    _animator.SetBool("Shoot", true);
                    _animator.SetBool("Run", false);
                }
            }
        }
        else
        {
            if (_navMeshAgent.enabled)                                          // if AI enabled
            {
                bool shoot = false;
                bool idle = false;
                bool follow = false;

                if (dist < FollowDistance)
                {
                    follow = true;
                    shoot = false;

                }

                if (dist < AttackDistance)
                {
                    follow = false;
                    shoot = true;

                    transform.LookAt(Player.transform);

                    Ray ray = new Ray(transform_RayOrigin.position, (Player.transform.position - transform_RayOrigin.position).normalized);
                    RaycastHit hit;
                    ray.direction *= 100;

                    if (Physics.Raycast(ray, out hit))
                    {
                        if (!hit.collider.CompareTag("Player"))
                        {
                            follow = true;
                            shoot = false;
                        }
                    }
                }

                if (follow)
                {
                    _navMeshAgent.SetDestination(Player.transform.position);
                    GunToAction();
                }

                if (!follow && shoot)
                {
                    _navMeshAgent.SetDestination(transform.position);
                    GunToAction();
                    transform.forward = Player.transform.position - transform.position;
                    transform.rotation = Quaternion.Euler(new Vector3(0, transform.rotation.eulerAngles.y, 0));
                }

                if (!follow && !shoot)
                {
                    idle = true;
                    GunToIdle();
                }

                _animator.SetBool("Shoot", shoot);
                _animator.SetBool("Run", follow);
                _animator.SetBool("Idle", idle);
            }
        }
    }

    public void OnShootAnimEnded()      // When Enemy Shoot animation loops this function Raises (Invokes)
    {
        int randomNumberForMuzzelFlash = Random.Range(0, 5);
        holdFlash = Instantiate(muzzelFlash[randomNumberForMuzzelFlash], muzzelSpawn.transform.position /*- muzzelPosition*/, muzzelSpawn.transform.rotation * Quaternion.Euler(0, 0, 90)) as GameObject;
        holdFlash.transform.parent = muzzelSpawn.transform;
        // Spawn Rand. Muzzle flashes

        if (GunSound)
            GunSound.Play();    //Play Shoot sound


        float random = Random.Range(0.0f, 1.0f);
        // The higher the accuracy is, the more likely the player will be hit
        bool isHit = random > 1.0f - HitAccuracy;

        if (isHit)  // Check roll success
            Player.GetComponent<IDamageable>().DamageObject(DamagePoints);
    }

    private void CheckObjectStatus()
    {
        if (stats.float_ObjectHealth <= 0)  // if health drops zero or less 
        {
            _navMeshAgent.SetDestination(transform.position);   // Stop moving
            GetComponent<Collider>().enabled = false;           // stop taking damage

            this.enabled = false;                               // Stop update

            _animator.SetBool("Shoot", false);                  // Set animator to death animation
            _animator.SetBool("Run", false);
            _animator.SetBool("Idle", false);

            _animator.SetTrigger("Death");

        }
    }

    public void OnDeathAnimationEnded()
    {
        gameObject.SetActive(false);  // On death anim ended Deactivade object
    }


    // Sets gun position/rotation to action anim
    private void GunToAction()
    {
        transform_Gun.localPosition = transform_gunAction.localPosition;
        transform_Gun.localRotation = transform_gunAction.localRotation;
    }


    // Sets gun position/rotation to idle anim
    private void GunToIdle()
    {
        transform_Gun.localPosition = transform_gunIdle.localPosition;
        transform_Gun.localRotation = transform_gunIdle.localRotation;
    }


    // For editor only to see attack(red)/ follow(green) range
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, FollowDistance);
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, AttackDistance);
    }
}