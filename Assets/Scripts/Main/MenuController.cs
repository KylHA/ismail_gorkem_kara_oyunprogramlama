using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;



public class MenuController : MonoBehaviour
{
    public static MenuController instance;

    [SerializeField] GameObject sceneCam, sceneLight;

    [SerializeField] Transform startText, finishText;
    [SerializeField] Transform textFinishPos;

    private void Awake()
    {
        if (instance != null)
            Debug.LogError("Multiple Instance");
        else
            instance = this;
    }

    Coroutine cor;
    private void Start()
    {

        cor = StartCoroutine(SmoothMove(startText, textFinishPos, 10f, () =>
          {
              SetMenuSceneElems();
              SceneManager.LoadSceneAsync(1, LoadSceneMode.Additive);
          }));
    }


    public void SetCredits()
    {
        SceneManager.UnloadSceneAsync(1);
        SetMenuSceneElems(true);
        cor = StartCoroutine(SmoothMove(finishText, textFinishPos, 10f, () =>
        {
        }));
    }

    private void SetMenuSceneElems(bool isActive = false) { sceneCam.SetActive(isActive); sceneLight.SetActive(isActive); }

    IEnumerator SmoothMove(Transform _moveObj, Transform finalTransform, float animTime, Action onComplete)
    {
        float deltaTime = 0;

        Vector3 startpos = _moveObj.position;

        while (deltaTime <= animTime)
        {

            float t = deltaTime / animTime;
            _moveObj.position = Vector3.Lerp(startpos, finalTransform.position, t);


            deltaTime += Time.deltaTime;
            yield return null;
        }

        onComplete?.Invoke();
    }
}
